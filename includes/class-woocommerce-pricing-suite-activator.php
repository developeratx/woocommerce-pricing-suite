<?php

/**
 * Fired during plugin activation
 *
 * @link       http://developeratx.com/woocommerce-pricing-suite
 * @since      1.0.0
 *
 * @package    Woocommerce_Pricing_Suite
 * @subpackage Woocommerce_Pricing_Suite/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Woocommerce_Pricing_Suite
 * @subpackage Woocommerce_Pricing_Suite/includes
 * @author     Craig Williams <plugins@developeratx.com>
 */
class Woocommerce_Pricing_Suite_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
