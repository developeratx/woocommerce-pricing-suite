<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://developeratx.com/woocommerce-pricing-suite
 * @since      1.0.0
 *
 * @package    Woocommerce_Pricing_Suite
 * @subpackage Woocommerce_Pricing_Suite/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
